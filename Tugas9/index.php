<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');


$sheep = new animal("shaun");

echo "name : ".$sheep->nama."<br>"; 
echo "legs : ".$sheep->legs."<br>"; 
echo "cold blooded :".$sheep->cold_blooded."<br>"."<br>"; 

$kodok = new frog("buduk");
echo "name : ".$kodok->nama."<br>"; 
echo "legs : ".$kodok->legs."<br>"; 
echo "cold blooded :".$kodok->cold_blooded."<br>"; 
echo "Jump :".$kodok->jump()."<br>"."<br>" ; 

$sungokong = new ape("kera sakti");
echo "name : ".$sungokong->nama."<br>"; 
echo "legs : ".$sungokong->legs."<br>"; 
echo "cold blooded :".$sungokong->cold_blooded."<br>"; 
echo "Yell : ".$sungokong->yell()."<br>"."<br>"; 

?>